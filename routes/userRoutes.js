const express = require('express');//to be able to use the Router() method

const router = express.Router();

const userControllers = require('../controllers/userControllers');

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUserController);

router.put('/:id', userControllers.updateUserUsernameController);

router.get('/getSingleUser/:id', userControllers.getsingleUserController);

module.exports = router;