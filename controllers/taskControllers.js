const Task = require('../models/Task');

module.exports.createTaskController = (req, res) => {
	console.log(req.body);

	Task.findOne({name: req.body.name}).then(result => {

		if(result !== null && result.name === req.body.name) {
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task({
				name : req.body.name,
				status : req.body.status
			})
			
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	}).catch(error => res.send(error));
};

module.exports.getAllTaskController = (req, res) => {
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.getSingleTaskController = (req, res) => {
	console.log(req.params);
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.updateTaskStatusController = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status : req.body.status
	};

	//{new : true} - allows us to return the updated version of the document we were updating, By default, without this argument, findByIdandUpdate will return the previous state of the document or previous version
	Task.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};