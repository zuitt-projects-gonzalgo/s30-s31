//Note: all packages/ modules should be require at the top of the file to avoid tampering or errors
const express = require('express'); //to load expressjs module

//Mongoose is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB
const mongoose = require('mongoose'); // to load mongoose module

//to import routes
const taskRoutes = require('./routes/taskRoutes');
const userRoutes = require('./routes/userRoutes');

const port = 4000; //to assign a port
const app = express(); //to create a server

/*
	syntax:
		mongoose.connect("<connectionsStringFromMongoDBAtlas>", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})
*/

mongoose.connect("mongodb+srv://admin_gonzalgo:admin169@gonzalgo-169.griu6.mongodb.net/task169?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//create notifications if the connection to the DB is succesfull or not
	let db = mongoose.connection;

	//we add this so that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client
	db.on('error', console.error.bind(console, 'Connection Error'));

	//once the connection is open and succesfull, we will output a message in the terminal
	db.once('open', () => console.log('Connected to MongoDB'));

//MIDDLEWARE
	app.use(express.json()); //to run express.json and automatically convert to json (middleware)

	//our server will use a middleware to group all task routes under /tasks
	//all the endpoints in taskRoutes file will start with /tasks
	app.use('/tasks', taskRoutes);

	app.use('/users', userRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}`)) //to assign the server to the port