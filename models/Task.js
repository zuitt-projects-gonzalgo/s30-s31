const mongoose = require('mongoose'); //to load mongoose module

//mongoose Schema
	//before we can create documents from our api to save into our database, we first have to determine the structure of the documents to be written in the database

	//a Schema is a blueprint for our document
	//Schema() is a constructor from mongoose that will allow us to create a new schema object
const taskSchema = new mongoose.Schema( //to declare the data type of the properties
	//defire the fields for our task document. the task document should have name and status fields.
	//both of the field should have a data type of string
	{
		name : String,
		status : String
	}
);

//Mongoose model
	//Models are used to connect your api to the corresponding collection in you database. It is a representation of the Task document.

	//Models used schema to create objects that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized

	//syntax: mongoos.model("<nameOfCollectionInAtlas>", <schemaToFollow>)

module.exports = mongoose.model("Task", taskSchema);

//module.exports will allows us to export files/ functions and be able to import/ require them in another file within our application

//Export the model into other files that may require it