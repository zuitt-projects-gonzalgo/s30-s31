const mongoose = require('mongoose'); //to load mongoose module

const userSchema = mongoose.Schema( //to declare data type of properties
	{
		username : String,
		password : String
	}
);

module.exports = mongoose.model("User", userSchema); //to allow to be exported 