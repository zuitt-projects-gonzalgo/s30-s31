const User = require('../models/User');

module.exports.createUserController = (req, res) => {
	console.log(req.body);

	User.findOne({username: req.body.username}).then(result => {

		if(result !== null && result.username === req.body.username) {
			return res.send('Duplicate task found')
		} else {
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})
			
			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	}).catch(error => res.send(error));
};

module.exports.getAllUserController = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.updateUserUsernameController = (req, res) => {
	let updates = {
		username : req.body.username
	};
	User.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.getsingleUserController = (req, res) => {
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}